package java8featuresnew;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.DoubleSummaryStatistics;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Java8FeaturesTest {

	private static final String ClassNAME = "inter";

	private static String addMethod() {
		return "inter";
	}

	public static void main(String[] args) {

		// TODO Auto-generated method stub
		List<Students> students = new ArrayList<>();
		Students studento = new Students();
        studento.setName("sai");
        studento.setClassName(addMethod());
        studento.setRollno("vcf123");
		studento.setMarks(20.00);
		students.add(studento);
		Predicate<Students> studPredicate = k -> {
			return k.getName().charAt(0) == 's';
		};

		// List<Students> KheerStudents =
		// students.stream().filter(x->x.getName().charAt(0).compare('s')).collect(Collectors.toList());

		List<Students> KStudents = students.stream().filter(studPredicate).collect(Collectors.toList());
		System.out.println(KStudents);
		for (Students st : KStudents) {
			System.out.println(st.getName() + "this");
		}

		Students student = new Students();
		student.setName("sai");
		student.setClassName(addMethod());
		student.setRollno("vcf123");
		student.setMarks(20.00);

		Students student2 = new Students();
		student2.setName("sai");
		student2.setClassName(addMethod());
		student2.setRollno("vcf124");
		student2.setMarks(90.00);

		Students studentone = new Students();
		studentone.setName("jc");
		studentone.setClassName(ClassNAME);
		studentone.setRollno("vcf125");
		studentone.setMarks(56.90);
		students.add(student);
		students.add(studentone);

		students.add(student2);
		Predicate<Students> studsPredicate = x -> {
			return x.getName().equals("sai") && x.getRollno().equals("vcf124");
		};

		List<Students> slist = students.stream().dropWhile(studsPredicate).collect(Collectors.toList());
		System.out.println(slist);
		List<Students> saiNames = students.stream()
				.filter(studPredicate)
				.collect(Collectors.toList());
		List<String> exMap4 = students.stream()
				.collect(Collectors.groupingBy(x -> x.getRollno()))
				.entrySet()
				.stream()
				.filter(x -> x.getKey().equals("vcf123"))
				.map(x -> x.getKey())
				.collect(Collectors.toList());

		boolean saiNamesList = students.stream()
				.anyMatch(studPredicate);
		System.out.println(saiNamesList);

		Optional<Students> optionalStudents = students.stream()
				.filter(studPredicate)
				.findAny();

		if (optionalStudents.isPresent()) {
			System.out.println(optionalStudents.get());
		}

		System.out.println(saiNames);

		saiNames.forEach(x -> System.out.println(x.getName()));

		List<String> noises = new ArrayList<>();
		String first = new String("sai");
		String second = new String("jc");

		List<String> heavyNoises = new ArrayList<>();
		String third = new String("tenth");
		String fourth = new String("inter");
		noises.add(first);
		noises.add(second);
		heavyNoises.add(third);
		heavyNoises.add(fourth);
		noises.addAll(heavyNoises);

		for (String noise : noises) {
			System.out.println(noise); // o/p :sai jc tenth inter
		}

		List<String> st = students.stream().collect(Collectors.groupingBy(x -> x.getRollno())).entrySet().stream()
				.filter(x -> x.getKey().equals("vcf123")).map(x -> x.getKey()).collect(Collectors.toList());

		System.out.println(st);

		Map<String, List<String>> exMap = new HashMap<>();
		exMap.put("tenth", noises);
		exMap.put("inter", heavyNoises);

		Map<String, List<String>> exMap2 = new HashMap<>();
		exMap2.put("tenth", noises);
		exMap2.put("inter", heavyNoises);

		List<Map<String, List<String>>> muchNoise = new ArrayList<>();
		muchNoise.add(exMap);
		muchNoise.add(exMap2);

		for (Map<String, List<String>> emo : muchNoise) {
			System.out.println(emo.get("tenth")); // o/p :[sai, jc, tenth, inter]
		}

		for (Map.Entry<String, List<String>> m : exMap.entrySet()) {
			m.getKey();
			System.out.println(m.getKey()); // o/p : tenth SECOND itteration o/p: inter
			System.out.println(m.getValue()); // o/p : [sai, jc, tenth, inter] SECOND itteration o/p:[tenth, inter]
		}

		// exMap.put(vcf123, value);

		/*
		 * 
		 * students.stream().collect(Collectors.groupingBy(x ->
		 * 
		 * x.getName())).entrySet().stream().filter(c ->
		 * 
		 * c.getKey().test(student)).foreach(x -> { System.out.println(""); });
		 * 
		 */

		Calendar cal = Calendar.getInstance();
		Date date = new Date();
		System.out.println(Calendar.getInstance());
		System.out.println(date);
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		System.out.println(cal.getTime());
		boolean name = students.stream().filter(a -> !(a.getName().isEmpty()))
				.collect(Collectors.groupingBy(a -> a.getName())).containsKey("k");

		System.out.println(name);

		List<String> names = students.stream().filter(a -> !(a.getName().isEmpty()))
				.collect(Collectors.groupingBy(a -> a.getName())).get("sai").stream()
				.filter(a -> !(a.getRollno().isEmpty())).map(s -> s.getName()).collect(Collectors.toList());

		System.out.println(names);

		DoubleSummaryStatistics stats = students.stream().filter(a -> !(a.getName().isEmpty()))
				.collect(Collectors.summarizingDouble(a -> a.getMarks()));

		System.out.println(stats);

		/*
		 * 
		 * List<Students> studentsinfo = new ArrayList<>(); Students studenttwo = new
		 * 
		 * Students(); studenttwo.setName("sai"); studenttwo.setClassName("tenth");
		 * 
		 * studenttwo.setRollno("vcf123"); studentsinfo.add(student);
		 * 
		 */

		Optional<Students> optionalStudent = students.stream().filter(stud -> stud.getName().equalsIgnoreCase("sai"))
				.findAny();

		if (optionalStudent.isPresent()) {
			System.out.println(true);
		}

		students.parallelStream().forEach(a -> {
			System.out.println(a);
		});

		/*
		 * 
		 * for (Students stud : students) { System.out.println(stud);
		 *
		 * 
		 * 
		 * }
		 * 
		 */

	}

}